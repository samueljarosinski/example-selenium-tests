package pl.allegro.sandbox.tests.navigation.navigators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pl.allegro.sandbox.tests.navigation.Navigator;

public class HeaderNavigator extends Navigator {

    public void insertSearchPhrase(String phrase) {
        WebElement queryElement = getDriver().findElement(By.name("string"));
        queryElement.clear();
        queryElement.sendKeys(phrase);
    }

    public void clickSearch() {
        getDriver()
                .findElement(By.xpath("/html/body/div[3]/div[2]/nav/div/div[1]/div/div/form/button"))
                .click();
    }

    public WebElement getSuggestions() {
        return getDriver().findElement(
                By.xpath("/html/body/div[3]/div[2]/nav/div/div[1]/div/div/form/div[1]/div[1]/div[1]"));
    }
}
