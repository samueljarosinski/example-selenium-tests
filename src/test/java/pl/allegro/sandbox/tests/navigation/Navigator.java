package pl.allegro.sandbox.tests.navigation;

import org.openqa.selenium.WebDriver;
import pl.allegro.sandbox.tests.driver.DriverController;

public class Navigator {

    protected WebDriver getDriver() {
        return DriverController.getInstance().getDriver();
    }
}
