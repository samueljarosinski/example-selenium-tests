package pl.allegro.sandbox.tests.navigation.navigators;

import org.openqa.selenium.By;
import pl.allegro.sandbox.tests.navigation.Navigator;

public class CategoryNavigator extends Navigator {

    public void goToElectronics() {
        getDriver().findElement(By.linkText("ELEKTRONIKA")).click();
    }
}
