package pl.allegro.sandbox.tests.navigation;

import org.openqa.selenium.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import pl.allegro.sandbox.tests.navigation.navigators.AllegroNavigator;
import pl.allegro.sandbox.tests.navigation.navigators.CategoryNavigator;
import pl.allegro.sandbox.tests.navigation.navigators.HeaderNavigator;
import pl.allegro.sandbox.tests.navigation.navigators.ListingNavigator;

public class NavigatorsProvider {

    private final static NavigatorsProvider INSTANCE = new NavigatorsProvider();

    private List<Navigator> navigators = new ArrayList<>();

    private NavigatorsProvider() {
        navigators.add(new AllegroNavigator());
        navigators.add(new CategoryNavigator());
        navigators.add(new HeaderNavigator());
        navigators.add(new ListingNavigator());
    }

    private <T extends Navigator> T getNavigator(Class<T> navigatorClass) {
        for (Navigator navigator : navigators) {
            if (navigator.getClass().equals(navigatorClass)) {
                //noinspection unchecked
                return (T) navigator;
            }
        }

        throw new NotFoundException("Navigator with given name was not found");
    }

    public static AllegroNavigator inAllegro() {
        return INSTANCE.getNavigator(AllegroNavigator.class);
    }

    public static CategoryNavigator inCategory() {
        return INSTANCE.getNavigator(CategoryNavigator.class);
    }

    public static HeaderNavigator inHeader() {
        return INSTANCE.getNavigator(HeaderNavigator.class);
    }

    public static ListingNavigator inListing() {
        return INSTANCE.getNavigator(ListingNavigator.class);
    }
}
