package pl.allegro.sandbox.tests.navigation.navigators;

import org.openqa.selenium.By;
import pl.allegro.sandbox.tests.navigation.Navigator;

public class AllegroNavigator extends Navigator {

    public void goToHomePage() {
        getDriver().get("https://allegro.pl.allegrosandbox.pl/");
        getDriver()
                .findElement(By.xpath("/html/body/div[5]/div/div[2]/div/div/div/div[2]/div/div[1]/button"))
                .click();
    }
}
