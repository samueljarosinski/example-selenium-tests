package pl.allegro.sandbox.tests.navigation.navigators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pl.allegro.sandbox.tests.navigation.Navigator;

public class ListingNavigator extends Navigator {

    public WebElement title() {
        return getDriver().findElement(By.className("listing-title__phrase"));
    }
}
