package pl.allegro.sandbox.tests.base;

import static pl.allegro.sandbox.tests.navigation.NavigatorsProvider.inAllegro;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import java.util.Arrays;
import pl.allegro.sandbox.tests.driver.DriverController;
import pl.allegro.sandbox.tests.driver.DriverController.Browser;

@RunWith(Parameterized.class)
public class BaseAllegroTest {

    @Parameters(name = "{0}")
    public static Iterable<?> browsers() {
        return Arrays.asList(Browser.CHROME, Browser.FIREFOX);
    }

    @Parameter
    public Browser browser;

    private DriverController driverController = DriverController.getInstance();

    @Before
    public void setUp() {
        driverController.start(browser);
        inAllegro().goToHomePage();
    }

    @After
    public void tearDown() {
        driverController.quit();
    }
}
