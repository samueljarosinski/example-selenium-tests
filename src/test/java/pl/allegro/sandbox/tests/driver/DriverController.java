package pl.allegro.sandbox.tests.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;

public class DriverController {

    public enum Browser {
        CHROME, FIREFOX
    }

    private WebDriver driver;

    private static final DriverController INSTANCE = new DriverController();

    public static DriverController getInstance() {
        return INSTANCE;
    }

    private DriverController() {}

    public WebDriver getDriver() {
        return driver;
    }

    public void start(Browser browser) {
        switch (browser) {
            case CHROME:
                driver = new ChromeDriver();

                break;
            case FIREFOX:
                driver = new FirefoxDriver();

                break;
        }

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
    }

    public void quit() {
        driver.quit();
    }
}
