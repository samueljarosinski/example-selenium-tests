package pl.allegro.sandbox.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static pl.allegro.sandbox.tests.navigation.NavigatorsProvider.inHeader;
import static pl.allegro.sandbox.tests.navigation.NavigatorsProvider.inListing;

import org.junit.Test;
import pl.allegro.sandbox.tests.base.BaseAllegroTest;

public class SearchTest extends BaseAllegroTest {

    @Test
    public void testSearch() {
        // given
        String phrase = "ps4";
        inHeader().insertSearchPhrase(phrase);

        // when
        inHeader().clickSearch();

        // then
        assertEquals(phrase, inListing().title().getText());
    }

    @Test
    public void testSuggestions() {
        // given
        String phrase = "pixel 2 xl";

        // when
        inHeader().insertSearchPhrase(phrase);

        // then
        assertTrue(inHeader().getSuggestions().isDisplayed());
    }
}
